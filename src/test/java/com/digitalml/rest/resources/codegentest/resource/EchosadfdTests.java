package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class EchosadfdTests {

	@Test
	public void testResourceInitialisation() {
		EchosadfdResource resource = new EchosadfdResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationRetrieveTestPathNoSecurity() {
		EchosadfdResource resource = new EchosadfdResource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveTestPath(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveNoSecurity() {
		EchosadfdResource resource = new EchosadfdResource();
		resource.setSecurityContext(null);

		Response response = resource.retrieve();
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateNameNoSecurity() {
		EchosadfdResource resource = new EchosadfdResource();
		resource.setSecurityContext(null);

		Response response = resource.createName(null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}